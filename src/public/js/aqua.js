/* global fetch */
/* global DOORSTATE */


//find by id helper
function $id(elementId) {
  return document.getElementById(elementId);
}

function clickedZone(name) {
  console.log("clicked: "+name);
  fetch(`/zone/${name}`).then(resp=>{
    return resp.json().then(json=>{
      console.log(json);
    });
  });
}

// get the state from the API
function checkState() {
  //fetch the state
  fetch("/state").then(resp=>{
    return resp.text().then(txt=>{
      //parse the text as json
      let jsn = JSON.parse(txt);
      //handle the state changing
      $id("status-state").textContent =jsn['Status'];

    });
  });
}

function looper() {
 // call looper in 1 second
 setTimeout(looper, 1000);
 //get the state
 checkState();
}

//load the config and make buttons
fetch("/state").then(resp=>{
  return resp.text().then(txt=>{
    //parse the text as json
    let jsn = JSON.parse(txt);
    console.log(jsn);
    let keys = Object.keys( jsn.Config.Zones);
    keys.forEach((k)=>{
      console.log(k);
      //make a button for the key
      var btn = document.createElement('button');
      //handle button click
      btn.onclick =function() {clickedZone(k);};
      btn.textContent=k;
      $id("zoneButtons").appendChild(btn);
    });
  });
});

//start the looper
looper();