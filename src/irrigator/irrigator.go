package irrigator

import(
  "log"
  "../ioPin"
  "../config"
  "time"
  "strconv"
)

type State struct {
  Data string
}

type Irrigator struct {
  pins map[string]ioPin.IoPin
  IrrigationTime int64
  currentZone string
}

func New(conf config.Config) *Irrigator {
  //make the pins map
  pins := make(map[string]ioPin.IoPin)

  //add the pump pin to the pins map
  pins["pump"] = ioPin.New("out", conf.Pump)

  //loop through the config's Zones
  for k,v := range conf.Zones {
    //add the zones as ioPins
    pins[k] = ioPin.New("out",v)
  }

  //make an instance of an Irrigator
  irr := Irrigator{pins, conf.IrrigationTime, ""}
  //ensure all pins are off
  irr.allPinsOff()
  return &irr
}
//handle shutting all pins off
func (irr *Irrigator) allPinsOff(){
  //ensure all pins are off
  for p := range irr.pins {
    irr.pins[p].Off()
  }
}

func (irr *Irrigator) Irrigate(zone string, duration string) (string, int64) {
  // is the zone valid?
  zonePin, ok := irr.pins[zone]
  if !ok {
    return "INVALID_ZONE",0
  }
  //is the currentZone already set?
  currentZonePin, ok := irr.pins[irr.currentZone]
  if ok {
    currentZonePin.Off()
  }
  // get an integer value from the duration
  minutes, err := strconv.ParseInt(duration, 10, 64)
  if (err!=nil) {
   //pffttt do nothing
  }
    // is there a duration greater than zero?
  if (minutes==0) {
    //set minutes to the default irrigation time
    minutes = irr.IrrigationTime
  }

  log.Printf("Irrigate Zone: %v for %v minutes\n", zone, minutes)
  //set the irrigator's currentZone
  irr.currentZone = zone
  //energize zone
  zonePin.On()
  //energize pump
  irr.pins["pump"].On()
  //end irrigation in another thread
  go irr.endIrrigation(minutes)
  return zone, minutes
}

func (irr *Irrigator) endIrrigation(minutes int64) {
  // track the zone
  targetZone := irr.currentZone
  //determine the end time
  endTime := time.Now().Add(time.Duration(minutes) * time.Minute)
  log.Println(endTime)
  for time.Now().Before(endTime) {
    //sleep for a bit
    time.Sleep( 10 * time.Millisecond )
    //if the target zone is no longer == irr.currentZone, there was an interrupt
    if irr.currentZone != targetZone {
      return;
    }
  }
  //shut off the pins
  irr.allPinsOff()
}

func (irr *Irrigator) Stop() {
  irr.currentZone = ""
  //shut off the pins
  irr.allPinsOff()
}