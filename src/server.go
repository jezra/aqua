/* copyright 2020 Jezra
released under GPLv3
*/

package main

import (
  "fmt"
  "net/http"
  "log"
  "strings"
  "path/filepath"
  "os"
  "encoding/json"
  "./config"
  "./irrigator"
)
/* global vars */
var conf config.Config
var state string
var publicDir string

var aqua *irrigator.Irrigator

// a struct to hand
type aquaState struct {
  Status string
  Config config.Config
}

func handleRoot(w http.ResponseWriter, r *http.Request) {
  path := r.URL.Path
  //if the path is "/", serve the index.html file
  if path=="/" {
    path="/index.html"
  }
  /* TODO: better sanitizing of the path */
  var newPath = filepath.Join(publicDir, path)
  http.ServeFile(w,r,newPath)
}

func handleState(w http.ResponseWriter, r *http.Request) {
  //get the aquaState object
  status := aquaState{Status: state, Config: conf}
  json, err := json.Marshal(status)
  if err!=nil {
   fmt.Fprint(w, "error")
  } else {
    statusStr := string(json)
    fmt.Fprintf(w, statusStr) // send data to client side
  }
}

func handleZone(w http.ResponseWriter, r *http.Request) {
  //split the Path
  pathParts := strings.Split(r.URL.Path, "/")
  zone := pathParts[2]
  duration :="0"
  if len(pathParts) > 3 {
    duration=pathParts[3]
  }
   z,m := aqua.Irrigate(zone, duration)
   //provide the response as a JSON compatible string
   resp := fmt.Sprintf("{\"zone\": \"%v\", \"minutes\": %v}", z,m)
  fmt.Fprintf(w, resp)
}

func handleStop(w http.ResponseWriter, r *http.Request) {
    aqua.Stop()
    //return the state
    handleState(w,r)
}

/* the main */
func main() {
  appDir, dErr := filepath.Abs(filepath.Dir(os.Args[0]))
  if dErr != nil {
    log.Fatal(dErr)
  }
  //load the config
  conf = config.New(appDir)
  //create an irrigator for the config
  aqua = irrigator.New(conf)


  // state is unknown until a command has been given
  state="init"
  //public dir (where to look for static assets)

  publicDir = appDir+"/public"
  //
  http.HandleFunc("/", handleRoot) // set root handler
  http.HandleFunc("/zone/", handleZone) // set close handler
  http.HandleFunc("/state", handleState) // set state handler
  http.HandleFunc("/stop", handleStop) // set state handler


  //inform the user of serving
  log.Printf("Serving on port: %v", conf.ServerPort)
  //start serving
  err := http.ListenAndServe(fmt.Sprintf(":%v",conf.ServerPort), nil) // set listen port
  if err != nil {
    log.Fatal("ListenAndServe: ", err)
  }
}

